//
//  CGRect+center.swift
//  Watch
//
//  Created by Anton Karachinskiy on 11/6/20.
//

import UIKit

extension CGRect {
    var center: CGPoint {
        return CGPoint(x: self.midX, y: self.midY)
    }
}
