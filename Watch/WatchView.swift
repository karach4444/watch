//
//  WatchView.swift
//  Watch
//
//  Created by Anton Karachinskiy on 10/22/20.
//

import UIKit

struct Time {
    var hour: Int
    var minute: Int
    var second: Int
    
    init(date: Date) {
        let calendar = Calendar.current
        hour = calendar.component(.hour, from: date)
        minute = calendar.component(.minute, from: date)
        second = calendar.component(.second, from: date)
    }
    
    init(hour: Int, minute: Int, second: Int) {
        self.hour = hour
        self.minute = minute
        self.second = second
    }
}

class WatchView: UIView {
    private var circleLayer: CAShapeLayer!
    private var secondHandLayer: CALayer!
    private var minuteHandLayer: CALayer!
    private var hourHandLayer: CALayer!
    private var isRunning = false
    private var time: Time = Time(date: Date())
    
    override func draw(_ rect: CGRect) {
        let lineWidth: CGFloat = 3
        let secondsAngle: CGFloat = CGFloat(time.second * (360 / 60))
        let minutesAngle: CGFloat = CGFloat(time.minute * (360 / 60))
        let hoursAngle: CGFloat = CGFloat(time.hour * (360 / 12)) + CGFloat(time.minute) * (1.0 / 60) * (360 / 12)
        let radius: CGFloat = (min(rect.width, rect.height) - lineWidth) / 2
        
        let path = UIBezierPath()
        path.lineWidth = lineWidth
        path.addArc(withCenter: rect.center, radius: radius, startAngle: 0, endAngle: 2 * .pi, clockwise: false)
        path.close()
        
        circleLayer = CAShapeLayer()
        circleLayer.path = path.cgPath
        circleLayer.fillColor = UIColor.white.cgColor
        circleLayer.strokeColor = UIColor.orange.cgColor
        
        secondHandLayer = CALayer()
        secondHandLayer.backgroundColor = UIColor.red.cgColor
        secondHandLayer.anchorPoint = CGPoint(x: 0.5, y: 0.2)
        secondHandLayer.position = rect.center
        secondHandLayer.bounds = CGRect(x: 0, y: 0, width: lineWidth, height: radius)
        
        minuteHandLayer = CALayer()
        minuteHandLayer.backgroundColor = UIColor.blue.cgColor
        minuteHandLayer.anchorPoint = CGPoint(x: 0.5, y: 0.2)
        minuteHandLayer.position = rect.center
        minuteHandLayer.bounds = CGRect(x: 0, y: 0, width: lineWidth, height: radius / 1.2)
        
        hourHandLayer = CALayer()
        hourHandLayer.backgroundColor = UIColor.black.cgColor
        hourHandLayer.anchorPoint = CGPoint(x: 0.5, y: 0.2)
        hourHandLayer.position = rect.center
        hourHandLayer.bounds = CGRect(x: 0, y: 0, width: lineWidth, height: radius / 1.5)
        
        secondHandLayer.transform = CATransform3DMakeRotation(.pi - secondsAngle / CGFloat(180 * Double.pi), 0, 0, 1)
        minuteHandLayer.transform = CATransform3DMakeRotation(.pi - minutesAngle / CGFloat(180 * Double.pi), 0, 0, 1)
        hourHandLayer.transform = CATransform3DMakeRotation(.pi - hoursAngle / CGFloat(180 * Double.pi), 0, 0, 1)
        
        layer.addSublayer(circleLayer)
        layer.addSublayer(secondHandLayer)
        layer.addSublayer(minuteHandLayer)
        layer.addSublayer(hourHandLayer)
        
        if isRunning {
            let secondsHandAnimation = CABasicAnimation(keyPath: "transform.rotation.z")
                    secondsHandAnimation.repeatCount = Float.infinity
                    secondsHandAnimation.duration = 60
                    secondsHandAnimation.isRemovedOnCompletion = false
            secondsHandAnimation.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.linear)
                    secondsHandAnimation.fromValue = (secondsAngle + 180) * CGFloat(Double.pi / 180)
                    secondsHandAnimation.byValue = 2 * Double.pi
                    secondHandLayer.add(secondsHandAnimation, forKey: "secondsHandAnimation")
            
            let minutesHandAnimation = CABasicAnimation(keyPath: "transform.rotation.z")
                    minutesHandAnimation.repeatCount = Float.infinity
                    minutesHandAnimation.duration = 60 * 60
                    minutesHandAnimation.isRemovedOnCompletion = false
                minutesHandAnimation.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.linear)
                    minutesHandAnimation.fromValue = (minutesAngle + 180) * CGFloat(Double.pi / 180)
                    minutesHandAnimation.byValue = 2 * Double.pi
                    minuteHandLayer.add(minutesHandAnimation, forKey: "minutesHandAnimation")
            
            let hoursHandAnimation = CABasicAnimation(keyPath: "transform.rotation.z")
                    hoursHandAnimation.repeatCount = Float.infinity
                    hoursHandAnimation.duration = 60 * 60 * 12
                    hoursHandAnimation.isRemovedOnCompletion = false
            hoursHandAnimation.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.linear)
                    hoursHandAnimation.fromValue = (hoursAngle + 180) * CGFloat(Double.pi / 180)
                    hoursHandAnimation.byValue = 2 * Double.pi
                    hourHandLayer.add(hoursHandAnimation, forKey: "hoursHandAnimation")
        }
    }
    
    func stopWatch() {
        if isRunning {
            guard let currentSecondHandTransform = secondHandLayer.presentation()?.value(forKeyPath: "transform.rotation.z") as? CGFloat,
            let currentMinuteHandTransform = minuteHandLayer.presentation()?.value(forKeyPath: "transform.rotation.z") as? CGFloat,
            let currentHourHandTransform = hourHandLayer.presentation()?.value(forKeyPath: "transform.rotation.z") as? CGFloat else {
                return
            }
            time = Time(date: Date())
            
            secondHandLayer.removeAnimation(forKey: "secondsHandAnimation")
            minuteHandLayer.removeAnimation(forKey: "minutesHandAnimation")
            hourHandLayer.removeAnimation(forKey: "hoursHandAnimation")
            secondHandLayer.transform = CATransform3DMakeRotation((currentSecondHandTransform), 0, 0, 1)
            minuteHandLayer.transform = CATransform3DMakeRotation((currentMinuteHandTransform), 0, 0, 1)
            hourHandLayer.transform = CATransform3DMakeRotation((currentHourHandTransform), 0, 0, 1)
            isRunning = false
        }
    }
    
    func startWatch() {
        if !isRunning {
            isRunning = true
            setNeedsDisplay()
        }
    }
    
    func setTime(time: Time) {
        self.time = time
    }
}
