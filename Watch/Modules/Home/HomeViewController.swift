//
//  HomeViewController.swift
//  Watch
//
//  Created by Anton Karachinskiy on 10/22/20.
//

import UIKit

class HomeViewController: UIViewController {
    @IBOutlet private weak var watch: WatchView!
    @IBOutlet private weak var stopwatch: WatchView!
    
    @IBAction private func stopWatch() {
        watch.stopWatch()
    }
    
    @IBAction private func startWatch() {
        watch.startWatch()
    }
    
    @IBAction private func startStopwatch() {
        stopwatch.setTime(time: Time(hour: 0, minute: 0, second: 0))
        stopwatch.startWatch()
    }
    
    @IBAction private func stopStopwatch() {
        stopwatch.stopWatch()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
    }
    
    private func setupView() {
        watch.startWatch()
        stopwatch.setTime(time: Time(hour: 0, minute: 0, second: 0))
        stopwatch.setNeedsDisplay()
    }

}

